# Changelog

### Version 1.0.1
Replaced tempfile command with mktemp

### Version 1.0.0
Initial release
